#!/usr/bin/env python3

def file_read(file_name):
    """Return file content.
    """
    content = None
    with open(file_name, 'r') as f:
        content = f.readlines()
    return content

def process_operator(operand_one, operand_two, operator):
    """ Fill this
    """
    pass

def parse_options():
    import argparse
    parser = argparse.ArgumentParser(prog='MySciCalc3',
                                     epilog="Copyright: (C) 2018",
                                     description='My First Python 3 Calculator')
    parser.add_argument('--input_file', help='user input file for this calculator')
    args = parser.parse_args()
    print(args.input_file)

# This is one line comment
def main():
    """
    This is skeleton function to learn more about python
    """
    from_name = "Python Pune"
    greetings = f"Hello Python 3 from {from_name}"
    print(greetings)
    parse_options()
    # TODO: Take user input for file name to process rather than
    # hard coding the values
    file_name = "./info.txt"
    # Add exception handling here
    user_input = file_read(file_name)
    print(user_input)
    for line in user_input:
        line = line.strip()
        operand_one, operator, operand_two = line.split(" ", 3)
        print(operand_two, operand_one, operator)

if __name__ == "__main__":
    main()
